import 'dart:math';
import 'package:flutter/material.dart';

final rand =  Random();
var activeDice = 'assets/images/dice-1.png';

class DiceRoller extends StatefulWidget {
  const DiceRoller({super.key});
  @override
  State<DiceRoller> createState() {
    return _DiceRollerState();
    // TODO: implement createState
    // throw UnimplementedError();
  }
}

class _DiceRollerState extends State<DiceRoller> {

  void roolDice() {
    // print('tes');
    setState(() {
      var num = rand.nextInt(6) + 1;
      activeDice = 'assets/images/dice-$num.png';
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Image.asset(
          activeDice,
          width: 200,
        ),
        TextButton(
          onPressed: roolDice,
          style: TextButton.styleFrom(
              foregroundColor: Colors.white,
              textStyle: const TextStyle(fontSize: 28)),
          child: const Text("tes"),
        )
      ],
    );
  }
}
