import 'package:flutter/material.dart';
import 'styled_text.dart';
import 'dice_roller.dart';

var start = Alignment.topLeft;
var end = Alignment.bottomRight;

class GradientCointainre extends StatelessWidget {
  const GradientCointainre(this.col1, this.col2, {super.key});

  const GradientCointainre.purple({super.key})
      : col1 = Colors.deepOrangeAccent,
        col2 = Colors.deepPurpleAccent;

  final Color col1;
  final Color col2;

  @override
  Widget build(context) {
    return Container(
      decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
        col1,
        col2,
      ], begin: start, end: Alignment.bottomRight)),
      child: const Center(
        child: DiceRoller(),
      ),
    );
  }
}

class QuizeScreen extends StatelessWidget {
  const QuizeScreen({super.key});

  final col1 = Colors.deepOrangeAccent;
  final col2 = Colors.deepPurpleAccent;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // throw UnimplementedError();
    return Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
          col1,
          col2,
        ], begin: start, end: Alignment.bottomRight)),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Opacity(
                opacity: 0.8,
                child: Image.asset(
                  'assets/images/quiz-logo.png',
                  width: 200,
                ),
              ),const SizedBox(
                height: 30,
              ),

              const Text("Lets Lean F", style: TextStyle(
                color: Colors.white, fontSize: 28
              ),
              ),
              OutlinedButton.icon(
                onPressed: () {},
                style: OutlinedButton.styleFrom(
                    foregroundColor: Colors.white,
                    ),
                icon: const Icon(Icons.arrow_right),
                label: const Text("Start Quiz"),
              )
            ],
          ),
        ));
  }
}
